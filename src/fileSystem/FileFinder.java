package fileSystem;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class FileFinder {

	public List<String> findAllJavaFiles(String dir) {
		List<String> files = new ArrayList<String>();
		files.addAll(findAllFiles(dir));
		Iterator<String> i = files.iterator();
		
		while (i.hasNext()) {
			String s = i.next();
			String[] split = s.split("\\.");
		
			if(!split[split.length-1].equals("java")){
				i.remove();
			}	
		}

		return files;
	}

	private List<String> findAllFiles(String dir) {
		File folder = new File(dir);
		List<String> files = new ArrayList<String>();

		for (final File fileEntry : folder.listFiles()) {
			if (fileEntry.isDirectory()) {
				files.addAll(findAllFiles(fileEntry.getAbsolutePath()));
			} else {
				files.add(fileEntry.getAbsolutePath());
			}
		}
		return files;
	}
}
