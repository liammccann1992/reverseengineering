package fileSystem;

import java.io.Closeable;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

public class FileWriter implements Closeable{

	private final PrintWriter out;
	public FileWriter(String path) throws FileNotFoundException{
		
		out = new PrintWriter(path); 
		
	}
	
	public void WriteLine(String s){
		out.println(s);
	}
	@Override
	public void close() throws IOException {
		out.close();
	}

}
