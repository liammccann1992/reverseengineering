package common;

import org.apache.commons.lang3.StringEscapeUtils;

public class StringHelper {

	public static String removeDotFileBreakers(String s){
		String toReturn = StringEscapeUtils.escapeJava(s);
		return toReturn.replaceAll("[^a-zA-Z0-9]", "");
	}
}
