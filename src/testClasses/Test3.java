package testClasses;

public class Test3 {

	private Test2 test2 = new Test2();

	public void metaCalculation() {
		long firstCalc = test2.calculate("Hi", 201);
		long secondCalc = test2.calculate("How", 309);
		long thirdCalc = test2.calculate("Are", 500);
		long fourthCalc = test2.calculate("You?", 1000);

		System.out.println("Result: " + firstCalc + secondCalc + thirdCalc
				+ fourthCalc);
	}

}
