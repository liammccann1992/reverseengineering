package testClasses;

public class Test2 {

	private int hereToTest = 1;
	private Test1 test1 = new Test1();

	public long calculate(String s, double lll) {
		long testResult = test1.testMethod(s, lll);

		System.out.println("Calculation: " + testResult);

		System.out.println(hereToTest);

		return testResult;
	}

}
