package model;

import java.util.ArrayList;
import java.util.List;

public class reAnalysis {
	private List<reClass> classes = new ArrayList<reClass>();

	public List<reClass> getClasses() {
		return classes;
	}

	public void setClasses(List<reClass> classes) {
		this.classes = classes;
	}

	public List<Dependency> getDependency() {
		List<Dependency> dps = new ArrayList<Dependency>();
		for (reClass clas : this.getClasses()) {
			dps.addAll(clas.getDependencies());
		}
		return dps;
	}
}
