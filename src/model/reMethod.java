package model;

import java.util.ArrayList;

public class reMethod {
	private String name;
	private String type;
	private ArrayList<reMethodParameter> parameters = new ArrayList<reMethodParameter>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public ArrayList<reMethodParameter> getParameters() {
		return parameters;
	}

	public void setParameters(ArrayList<reMethodParameter> parameters) {
		this.parameters = parameters;
	}

	@Override
	public String toString() {
		String toReturn = "\t\t Name: " + name + " \t\t Return Type: " + type + "\n";

		toReturn += ("\t\t\t -Parameters- \n");
		for (reMethodParameter parameter : parameters) {
			toReturn += "\t\t\t\t Name: " + parameter.name + " \t\t Type: "
					+ parameter.type + "\n";
		}

		return toReturn;
	}
}
