package model;

import java.util.ArrayList;
import java.util.List;

import reJavaParser.Common;

/**
 * A model of a class being analysed, holds data about the class to be analysed
 * later.
 * 
 * @author Damien Anderson (Damorin)
 * @author Liam McCann
 */
public class reClass {
	private String name;
	private List<reMethod> methods = new ArrayList<reMethod>();
	private List<reField> fields = new ArrayList<reField>();
	private List<Dependency> dependencies = new ArrayList<Dependency>();

	private String[] lines;
	private int NumberOfLines = 0;

	/**
	 * Defines a single {@link Dependency} for this class.
	 */
	public void buildDependency() {
		List<String> javaTypes = Common.getJavaTypes();
		for (reField field : fields) {
			String type = field.getType();
			if (!javaTypes.contains(type)) {
				dependencies.add(new Dependency(type, field.getName(), this
						.getName()));
			}
		}
	}

	public String[] getLines() {
		return this.lines;
	}

	public void setLines(String[] lines) {
		this.lines = lines;
	}

	public int getNumberOfLines() {
		return NumberOfLines;
	}

	public void setNumberOfLines(int numberOfLines) {
		NumberOfLines = numberOfLines;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<reMethod> getMethods() {
		return this.methods;
	}

	public List<Dependency> getDependencies() {
		return dependencies;
	}

	public void setMethods(ArrayList<reMethod> methods) {
		this.methods = methods;
	}

	public List<reField> getFields() {
		return this.fields;
	}

	public void setFields(ArrayList<reField> fields) {
		this.fields = fields;
	}

	@Override
	public String toString() {
		String toReturn = "Class Name: " + this.getName() + " (Lines#"
				+ NumberOfLines + ") \n";

		if (!this.getFields().isEmpty()) {
			toReturn += ("\t -Fields- \n");
			for (reField field : this.getFields()) {

				toReturn += field.toString();
			}
		}

		if (!this.getDependencies().isEmpty()) {
			toReturn += ("\t -Dependencies- \n");
			for (Dependency dependency : this.getDependencies()) {

				toReturn += "\t\t Name: " + dependency.getDeclaration()
						+ "\t\t Type: " + dependency.getType()
						+ "\t\t Strength: " + dependency.getStrength() + "\n";
			}
		}

		if (!this.getMethods().isEmpty()) {
			toReturn += ("\t -Methods- \n");
			for (reMethod method : this.getMethods()) {

				toReturn += method.toString();
			}
		}

		return toReturn;
	}
}
