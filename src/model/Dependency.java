package model;

/**
 * Represents a relation associated to a parent class. Stores the type of class
 * as well as its declaration.
 * 
 * @author Damien Anderson (Damorin)
 * @author Liam McCann
 */
public class Dependency {
	private String type;
	private String declaration;
	private String dependant;
	private int timesUsed = 0;
	private int methodsCalled = 0;
	private int totalMethods = 0;
	private int strength;

	public Dependency(String type, String declaration, String dependant) {
		setType(type);
		setDeclaration(declaration);
		setDependant(dependant);
	}

	public void increaseMethodsCalled() {
		this.methodsCalled++;
	}

	public String getDependant() {
		return dependant;
	}

	private void setDependant(String dependant) {
		this.dependant = dependant;
	}

	public String getType() {
		return type;
	}

	private void setType(String type) {
		this.type = type;
	}

	public String getDeclaration() {
		return declaration;
	}

	private void setDeclaration(String declaration) {
		this.declaration = declaration;
	}

	public int getStrength() {
		return strength;
	}

	public void increaseTimesUsed() {
		this.timesUsed++;
		this.calculateStrength();
	}

	private void calculateStrength() {
		int score = timesUsed;
		score = score * methodsCalled;
		this.strength = score;
	}
}
