package ui;

import japa.parser.ParseException;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import model.reAnalysis;
import model.reClass;
import reJavaParser.CuPrinter;
import reJavaParser.DuplicateFinder;
import reJavaParser.Grapher;
import reJavaParser.iGrapher;
import fileSystem.FileFinder;

public class AnalyserFrame extends InternalFrame implements ActionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JButton startIndexing;
	JList<String> listFoundFilesUI;
	JScrollPane scrollPane;
	JFileChooser fc;

	JButton createGraph;
	JButton showDuplicates;

	List<String> javaClasses;
	reAnalysis result;

	public AnalyserFrame() {
		super("Analyser");
		this.setLayout(null);
		this.setSize(700, 400);
		this.resizable = false;

		// Create a file chooser
		fc = new JFileChooser();

		startIndexing = new JButton("Choose Directory to Analyse");
		startIndexing.setBounds(10, 10, 670, 30);
		startIndexing.setActionCommand("Start");
		startIndexing.addActionListener(this);
		this.add(startIndexing);

		listFoundFilesUI = new JList<String>();
		scrollPane = new JScrollPane(listFoundFilesUI);
		scrollPane.setBounds(10, 50, 670, 250);

		createGraph = new JButton("Export .DOT file");
		createGraph.setBounds(10, 310, 200, 30);
		createGraph.setActionCommand("export");
		createGraph.addActionListener(this);
		this.add(createGraph);

		showDuplicates = new JButton("Find Duplicates");
		showDuplicates.setBounds(220, 310, 200, 30);
		showDuplicates.setActionCommand("duplicates");
		showDuplicates.addActionListener(this);
		this.add(showDuplicates);

		this.add(scrollPane);

	}

	public void Analyse() {
		CuPrinter cu = new CuPrinter(javaClasses);
		try {
			result = cu.startAnalysing();
		} catch (IOException e) {
			ShowErrorMessage("Error reading Java Files");
		} catch (ParseException e) {
			e.printStackTrace();
			ShowErrorMessage("Some of your Java source code has errors, Please fix these an try again");
		}

	}

	public void ShowErrorMessage(String msg) {
		JOptionPane.showMessageDialog(this, msg, "An Error has occurred",
				JOptionPane.ERROR_MESSAGE);
	}

	public void ShowInfoMesssage(String title, String msg) {
		JOptionPane.showMessageDialog(this, msg, title,
				JOptionPane.INFORMATION_MESSAGE);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		System.out.println("Result " + result + " JC " + javaClasses + " COD  " + e.getActionCommand());
		if(!e.getActionCommand().equals("Start") && (javaClasses == null || result == null)){
			ShowErrorMessage("Please run analyzing before trying to export/find duplicates");
			return;
		}
		if (e.getActionCommand().equals("Start")) {
			try {

				javaClasses = new ArrayList<String>();
				result = null;

				// Hanlde FIle UI
				File workingDirectory = new File(System.getProperty("user.dir"));
				fc.setCurrentDirectory(workingDirectory);
				fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				int returnVal = fc.showOpenDialog(AnalyserFrame.this);

				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File file = fc.getSelectedFile();
					// This is where a real application would open the file.
					System.out.println("Opening: " + file.getName());

					FileFinder ff = new FileFinder();
					String idxToSearch = file.getAbsolutePath();
					javaClasses = ff.findAllJavaFiles(idxToSearch);

					if (javaClasses.isEmpty()) {
						ShowErrorMessage("No .java files found within: \n "
								+ idxToSearch);
					} else {
						DefaultListModel<String> model = new DefaultListModel<String>();

						for (String s : javaClasses) {
							model.addElement(s.substring(file.getAbsolutePath()
									.length(), s.length()));
						}

						listFoundFilesUI.setModel(model);

						Analyse();
					}

				} else {
					System.out.println("Open command cancelled by user.");
				}

			} catch (Exception e1) {
				e1.printStackTrace();
			}
		} else if (e.getActionCommand().equals("duplicates")) {
			DuplicateFinder df = new DuplicateFinder(result.getClasses());
			Map<String, Integer> popLines = df.getMostPopularLine();
			String msg = "";
			for (Map.Entry<String, Integer> entry : popLines.entrySet()) {
				msg += "'" + entry.getKey() + "' appears " + entry.getValue()
						+ " times. \n";
			}
			ShowInfoMesssage("Duplicated Lines", msg);
			
		} else if (e.getActionCommand().equals("export")) {
			File workingDirectory = new File(System.getProperty("user.dir"));
			fc.setCurrentDirectory(workingDirectory);
			fc.setFileSelectionMode(JFileChooser.SAVE_DIALOG);
			int returnVal = fc.showSaveDialog(AnalyserFrame.this);

			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = fc.getSelectedFile();
				// This is where a real application would open the file.

				iGrapher ghp = new Grapher();
				try {
					String filePath = file.getAbsolutePath() + "\\ClassDiagram"
							+ GregorianCalendar.getInstance().getTimeInMillis()
							+ ".dot";
					System.out.println();
					ghp.BuildDotFile(result, filePath);
					String msg = "Your file has been created, Please open this in a .DOT file viewer such as GVEdit.";

					msg += "\n\n " + filePath;

					ShowInfoMesssage("Export Successful", msg);

				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			} else {
				System.out.println("Open command cancelled by user.");
			}

		}

	}
}
