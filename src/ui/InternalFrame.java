package ui;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class InternalFrame extends JInternalFrame{
	static int openFrameCount = 0;
    static final int xOffset = 30, yOffset = 30;
    
	public InternalFrame(String name) {
	    super(name,
	          true, //resizable
	          true, //closable
	          true, //maximizable
	          true);//iconifiable
	    //...Create the GUI and put it in the window...
	    //...Then set the window size or call pack...
	    
	    //Set the window's location.
	    setLocation(xOffset*openFrameCount, yOffset*openFrameCount);
	    setVisible(true);
	    

	   
	}
}
