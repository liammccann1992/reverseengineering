package reJavaParser;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import model.reClass;

/**
 * Searches the given files for duplicated lines and displays areas of potential
 * improvement.
 * 
 * @author Damien Anderson (Damorin)
 * @author Liam McCann
 */
public class DuplicateFinder implements iDuplicateFinder{
	private static final int NUMBER_OF_OCCURRANCES = 1;

	private Map<String, Integer> usedLines;

	public DuplicateFinder(List<reClass> classes) {
		usedLines = new HashMap<String, Integer>();

		for (reClass instance : classes) {
			for (String line : instance.getLines()) {
				if (line.trim().length() < 3)
					continue;
				line = line.trim();
				if (usedLines.containsKey(line)) {
					usedLines.put(line, usedLines.get(line) + 1);
				} else {
					usedLines.put(line, 1);
				}

			}
		}

		System.out.println("Number of unique lines: " + usedLines.size());
		
	}

	public Map<String, Integer> getMostPopularLine() {
		ValueComparator bvc = new ValueComparator(usedLines);
		TreeMap<String, Integer> sortedMap = new TreeMap<String, Integer>(bvc);
		sortedMap.putAll(usedLines);

		System.out.println("Lines with more than " + NUMBER_OF_OCCURRANCES
				+ " occurrances: ");

		Map<String, Integer> toReturn = new TreeMap<String,Integer>();
		
		for (Map.Entry<String, Integer> entry : sortedMap.entrySet()) {
			String key = entry.getKey();
			Integer value = entry.getValue();

			if (value > NUMBER_OF_OCCURRANCES)
				toReturn.put(key, value);
				
		}

		return toReturn;
	}
}

class ValueComparator implements Comparator<String> {

	private Map<String, Integer> base;

	public ValueComparator(Map<String, Integer> base) {
		this.base = base;
	}

	// Note: this comparator imposes orderings that are inconsistent with
	// equals.
	public int compare(String a, String b) {
		if (base.get(a) >= base.get(b)) {
			return -1;
		} else {
			return 1;
		} // returning 0 would merge keys
	}
}
