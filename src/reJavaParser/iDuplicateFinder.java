package reJavaParser;

import java.util.Map;

public interface iDuplicateFinder {
	public Map<String, Integer> getMostPopularLine();
}
