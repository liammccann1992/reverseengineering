package reJavaParser;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;

import common.StringHelper;

import model.Dependency;
import model.reAnalysis;
import model.reClass;
import model.reField;
import model.reMethod;
import fileSystem.FileWriter;

public class Grapher implements iGrapher {
	@Override
	public void BuildDotFile(reAnalysis analysis, String path)
			throws FileNotFoundException, IOException {

		System.out.println("Path: " + path);
		System.out.println("Building dot file");
		System.out.println("Classes #" + analysis.getClasses().size());
		FileWriter fwh = new FileWriter(path);
		fwh.WriteLine("digraph G {");
		fwh.WriteLine(" fontname = \"Bitstream Vera Sans\"");
		fwh.WriteLine("fontsize = 8");

		for (reClass cls : analysis.getClasses()) {
			fwh.WriteLine("node [shape = \"record\"]");
			fwh.WriteLine("edge [ ]");

			String fieldStr = BuildFieldsString(cls.getFields());
			String methodStr = BuildMethodsString(cls.getMethods());

			fwh.WriteLine(cls.getName() + " [label = \"{" + cls.getName()
					+ fieldStr + " " + methodStr + "}\"]");

		}

		for (Dependency dpc : analysis.getDependency()) {
			System.out.println(StringHelper.removeDotFileBreakers(dpc.getType()) + " -> " + dpc.getDependant() + ";");
			fwh.WriteLine("edge [label = " + dpc.getStrength() +  "]");
			fwh.WriteLine(StringHelper.removeDotFileBreakers(dpc.getType()) + " -> " + dpc.getDependant() + ";");
		}

		fwh.WriteLine("}");
		fwh.close();
	}

	private String BuildMethodsString(List<reMethod> methods) {
		if (methods.size() == 0)
			return "";

		String methodStr = "|";
		for (reMethod mth : methods) {
			methodStr += "+ " + mth.getName() + "() : " + StringEscapeUtils.escapeJava(mth.getType())
					+ " \\l";
		}
		return methodStr;
	}

	private String BuildFieldsString(List<reField> fields) {
		if (fields.size() == 0)
			return "";

		String fieldStr = "|";
		for (reField fld : fields) {
			fieldStr += "- " + fld.getName() + " : " + StringEscapeUtils.escapeJava(fld.getType()) + " \\l";
		}

		return fieldStr;
	}

}
