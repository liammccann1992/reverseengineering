package reJavaParser;

import japa.parser.JavaParser;
import japa.parser.ParseException;
import japa.parser.ast.CompilationUnit;
import japa.parser.ast.body.ClassOrInterfaceDeclaration;
import japa.parser.ast.body.FieldDeclaration;
import japa.parser.ast.body.MethodDeclaration;
import japa.parser.ast.body.Parameter;
import japa.parser.ast.body.VariableDeclarator;
import japa.parser.ast.expr.VariableDeclarationExpr;
import japa.parser.ast.stmt.BlockStmt;
import japa.parser.ast.stmt.Statement;
import japa.parser.ast.visitor.VoidVisitorAdapter;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;

import com.sun.xml.internal.ws.util.StringUtils;

import model.Dependency;
import model.reAnalysis;
import model.reClass;
import model.reField;
import model.reMethod;
import model.reMethodParameter;
import fileSystem.FileFinder;
public class CuPrinter {

	private List<String> classFiles;

	public CuPrinter(List<String> _classFiles) {
		classFiles = _classFiles;
	}

	public reAnalysis startAnalysing() throws IOException, ParseException {
		reAnalysis findings = new reAnalysis();
		List<reClass> classes = new ArrayList<reClass>();
		for (String f : classFiles) {
			System.out.println("Analysing: " + f);
			reClass analyseClass = analyseClass(f);
			classes.add(analyseClass);
		}
		findings.setClasses(classes);
		return findings;
	}

	public reClass analyseClass(String file) throws ParseException, IOException {
		CompilationUnit cu;
		FileInputStream in = new FileInputStream(file);
		try {
			cu = JavaParser.parse(in);
		} finally {
			in.close();
		}

		reClass parseClass = new reClass();
		parseClass.setNumberOfLines(cu.toString().split("\n").length);
		MethodVisitor visitor = new MethodVisitor(parseClass);
		visitor.visit(cu, null);

		parseClass.setLines(cu.toString().split("\\r?\\n"));

		return parseClass;
	}

	private class MethodVisitor extends VoidVisitorAdapter<Object> {
		reClass toParse;

		public MethodVisitor(reClass parseClass) {
			toParse = parseClass;
		}

		@Override
		public void visit(MethodDeclaration methodToAnalyse, Object arg) {
			reMethod method = new reMethod();
			method.setName(methodToAnalyse.getName());
			method.setType(StringEscapeUtils.escapeJava(methodToAnalyse.getType().toString()));
			
			List<Parameter> parameters = methodToAnalyse.getParameters();
			if (parameters != null) {
				for (Parameter parameter : parameters) {
					reMethodParameter param = new reMethodParameter();
					param.name = parameter.getId().toString();
					param.type = StringEscapeUtils.escapeJava(parameter.getType().toString());
					method.getParameters().add(param);
				}
			}

			toParse.getMethods().add(method);
			//System.out.println("###" + methodToAnalyse);
			determineDependencyStrength(methodToAnalyse);

		}

		private void determineDependencyStrength(MethodDeclaration n) {
			List<String> methodsUsed = new ArrayList<String>();
			if (!toParse.getDependencies().isEmpty()) {
				BlockStmt body = n.getBody();
				for (Dependency dependency : toParse.getDependencies()) {
					if (body == null)
						continue;
					List<Statement> stmts = body.getStmts();
					if (stmts == null)
						continue;

					for (Statement statement : body.getStmts()) {
						if (dependencyIsCalled(dependency, statement)) {
							String mthName = statementMethodName(statement);
							if (mthName != null
									&& !methodsUsed.contains(mthName)) {
								methodsUsed.add(statementMethodName(statement));
								dependency.increaseMethodsCalled();
							}
							dependency.increaseTimesUsed();
						}
					}
				}
			}
		}

		private String statementMethodName(Statement statement) {
			String ste = statement.toString();
			int begin = statement.getBeginLine();
			int indxBrac = statement.toString().indexOf("(");

			if (begin < 0 || indxBrac < 0 || begin > ste.length() || indxBrac > ste.length())
				return null;

			if (begin > indxBrac)
				return ste.substring(indxBrac, begin);
			else
				return ste.substring(begin, indxBrac);

		}

		private boolean dependencyIsCalled(Dependency dependency,
				Statement statement) {
			return statement.toString().contains(dependency.getDeclaration());
		}

		@Override
		public void visit(VariableDeclarationExpr n, Object arg) {
			List<VariableDeclarator> myVars = n.getVars();
			for (VariableDeclarator vars : myVars) {
				System.out.println("Variable Name: " + vars.getId().getName());
			}
		}

		@Override
		public void visit(FieldDeclaration n, Object arg) {
			reField field = new reField();
			String declarationName = n.getVariables().get(0).toString();
			addAppropriateName(field, declarationName);
			field.setType(n.getType().toString());
			toParse.getFields().add(field);
			toParse.buildDependency();
			super.visit(n, arg);
		}

		private void addAppropriateName(reField field, String declarationName) {
			if (declarationName.contains(" ")) {
				field.setName(getFirstWordOf(declarationName));
			} else {
				field.setName(declarationName);
			}
		}

		private String getFirstWordOf(String declarationName) {
			return declarationName.substring(0, declarationName.indexOf(" "));
		}

		@Override
		public void visit(ClassOrInterfaceDeclaration n, Object arg) {
			reClass classToInvestigate = new reClass();
			classToInvestigate.setName(n.getName());
			toParse.setName(classToInvestigate.getName());

			super.visit(n, arg);

		}
	}
}