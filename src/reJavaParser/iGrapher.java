package reJavaParser;

import java.io.FileNotFoundException;
import java.io.IOException;
import model.reAnalysis;

public interface iGrapher {
	public void BuildDotFile(reAnalysis analysis, String path) throws FileNotFoundException, IOException;
}
