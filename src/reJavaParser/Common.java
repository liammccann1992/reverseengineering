package reJavaParser;

import java.util.ArrayList;
import java.util.List;

/**
 * A list of standard java data types which will be ignored for the dependency
 * finder.
 * 
 * @author Damien Anderson (Damorin)
 * @author Liam McCann
 *
 */
public class Common {
	public static List<String> getJavaTypes() {
		List<String> types = new ArrayList<String>() {
			private static final long serialVersionUID = 1L;

			{
				add("byte");
				add("int");
				add("Integer");
				add("String");
				add("long");
				add("double");
				add("short");
				add("boolean");
				add("char");
				add("float");
			}
		};

		return types;
	}

}
